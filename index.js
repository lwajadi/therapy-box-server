const express = require("express");
const app = express();

const cors = require("./middleware/cors");

require("dotenv").config();

// Routes
const authRoute = require("./routes/auth");
const tasksRoute = require("./routes/tasks");

// Middleware
app.use(cors);
app.use(express.json());
app.use("/api/user", authRoute);
app.use("/api/tasks", tasksRoute);

app.listen(process.env.PORT || 80, () => console.log("Server Running"));
