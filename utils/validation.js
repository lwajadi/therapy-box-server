const Joi = require("@hapi/joi");

const loginValidation = data => {
  const loginUserSchema = Joi.object({
    username: Joi.string()
      .min(6)
      .required(),
    password: Joi.string()
      .min(6)
      .required()
  });
  return loginUserSchema.validate(data);
};

const registerValidation = data => {
  const registerUserSchema = Joi.object({
    username: Joi.string()
      .min(6)
      .required(),
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  });
  return registerUserSchema.validate(data);
};

const createTaskValidation = data => {
  const createTaskSchema = Joi.object({
    text: Joi.string().required(),
    checked: Joi.bool()
  });
  return createTaskSchema.validate(data);
};

module.exports.registerValidation = registerValidation;

module.exports.loginValidation = loginValidation;

module.exports.createTaskValidation = createTaskValidation;
