const router = require("express").Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { config } = require("../utils/db");

const { registerValidation, loginValidation } = require("../utils/validation");

// Register User

router.post("/register", async (req, res) => {
  try {
    // Validate user information
    const { error } = registerValidation(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    // Initialise connection with db
    const mysql = require("mysql2/promise");
    const conn = await mysql.createConnection(config);

    // Check if email already exists in the db. If not, return 400
    const [emailExists] = await conn.execute(
      `SELECT * FROM users WHERE email = "${req.body.email}";`
    );

    if (emailExists.length > 0)
      return res.status(400).send("This email already exists");

    // Check if username already exists in the db. If not, return 400
    const [usernameExists] = await conn.execute(
      `SELECT * FROM users WHERE username = "${req.body.username}";`
    );

    if (usernameExists.length > 0)
      return res
        .status(400)

        .send("This username already exists");

    // Hash the password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const user = {
      username: req.body.username,
      email: req.body.email,
      password: hashPassword
    };

    // Write to database
    await conn.execute(
      `INSERT INTO users (username,email,password) VALUES ("${user.username}","${user.email}","${user.password}");`
    );

    // Get the id of the newly created user to be included in token
    const [newId] = await conn.execute(`SELECT LAST_INSERT_ID()`);

    console.log(newId[0]["LAST_INSERT_ID()"]);
    // Create JWT using the newId
    const token = jwt.sign(
      { id: newId[0]["LAST_INSERT_ID()"] },
      process.env.TOKEN_SECRET
    );

    // Write to header following convention
    res
      .header("Access-Control-Expose-Headers", "Authorization")
      .header("Authorization", "Bearer " + token)
      .send(token);
    // TODO
  } catch (err) {
    res.status(400).send(err);
  }
});

// Login User

router.post("/login", async (req, res) => {
  // Validate user information
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Initialise connection with db
  const mysql = require("mysql2/promise");
  const conn = await mysql.createConnection(config);

  // Get user data using request parameters
  const [userRequest] = await conn.execute(
    `SELECT * FROM users WHERE username = "${req.body.username}";`
  );
  const user = userRequest[0];
  if (!user)
    return res.status(400).send("A user with this username does not exist");

  // Password is correct
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send("Invalid password");

  const token = jwt.sign({ id: user.id }, process.env.TOKEN_SECRET);

  res
    .header("Access-Control-Expose-Headers", "Authorization")
    .header("Authorization", "Bearer " + token)
    .send();
});

module.exports = router;
