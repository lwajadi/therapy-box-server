const router = require("express").Router();
const verify = require("../middleware/verify");

const { config } = require("../utils/db");

const { createTaskValidation } = require("../utils/validation");

// Create new
router.post("/new", verify, async (req, res) => {
  try {
    const { error } = createTaskValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const mysql = require("mysql2/promise");
    const conn = await mysql.createConnection(config);

    await conn.execute(
      `INSERT INTO tasks (text, checked, users_id) VALUES ("${req.body.text}",${req.body.checked},${req.user.id});`
    );
    const [newId] = await conn.execute(`SELECT LAST_INSERT_ID()`);

    console.log({ id: newId[0]["LAST_INSERT_ID()"] });
    res.send({ id: newId[0]["LAST_INSERT_ID()"] });
  } catch (err) {
    res.status(400).send("Server Error");
  }
});

// Get list of tasks
router.get("/list", verify, async (req, res) => {
  try {
    const mysql = require("mysql2/promise");
    const conn = await mysql.createConnection(config);

    const [tasksList] = await conn.execute(
      `SELECT * FROM tasks WHERE users_id = "${req.user.id}";`
    );
    console.log(tasksList);
    console.log({
      body: {
        tasks: tasksList
      }
    });
    res.header("Access-Control-Allow-Origin", "*").send({
      body: {
        tasks: tasksList
      }
    });
  } catch (err) {
    res.status(400).send("Error");
  }
});

// Toggle existing - to implement
router.post("/toggle", verify, async (req, res) => {
  try {
    const mysql = require("mysql2/promise");
    const conn = await mysql.createConnection(config);
  } catch (err) {
    res.status(400).send("Error");
  }
});

module.exports = router;
