module.exports = function(req, res, next) {
  res.header(
    "Access-Control-Allow-Origin",
    "https://angry-cori-ea2405.netlify.com"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  next();
};
