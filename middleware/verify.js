const jwt = require("jsonwebtoken");

module.exports = function(req, res, next) {
  const token = req.header("Authorization");
  if (!token) return res.status(401).send("Access Denied");

  if (!token.includes("Bearer ")) return res.status(401).send("Access Denied");

  try {
    const pureToken = token.replace("Bearer ", "");
    const verified = jwt.verify(pureToken, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    res.status(400).send("Invalid Token");
  }
};
